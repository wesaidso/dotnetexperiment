using System;

namespace SpecFlowCalculator.Specs.API;

public class GlobalStateHolder<T> : Cleanable
{
    private int _counter = 0;

    private T _last;
    // private Map<String, <Optional<T>> _states;

    public GlobalStateHolder()
    {
        Init();
    }

    public void Add(T item)
    {
        Add((_counter=_counter+1).ToString(), item);    
    }

    public void Add(String key, T item)
    {
        // _states.add(key, item);
        _last = item;
    }

    public T GetLast()
    {
        return _last;
    }
    
    private void Init() {
        // _states.clear();
        _last = default(T); //Is dit null?
        _counter = 1;
    }

    public T Get(String key)
    {
        // if (_states.IsEmpty())
        // {
        //     // throw IllegalStateException("State is empty, no items present");
        // }
        //
        // // return _states.get(key).orElse(null)

        return _last;
    }

    public void Clean()
    {
        Init();
    }
}