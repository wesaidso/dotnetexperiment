using System.Collections.ObjectModel;

namespace SpecFlowCalculator.Specs.API;

public class CleanableCleaner
{
    public CleanableCleaner(Collection<Cleanable> cleanables)
    {
        foreach (var cleanable in cleanables)
        {
            cleanable.Clean();
        }
    }
}

public interface Cleanable
{
    void Clean();
}