﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using SpecFlowCalculator.Specs.API;
using TechTalk.SpecFlow;

namespace SpecFlowCalculator.Specs.Steps
{
    [Binding]
    public sealed class DateTimeStepDefinitions
    {
        private readonly TimeTraveller _timeTraveller;

        public DateTimeStepDefinitions(TimeTraveller timeTraveller)
        {
            _timeTraveller = timeTraveller;
        }

        [Given("today is {date}")]
        public void GivenTheFirstNumberIs(DateOnly date)
        {
            _timeTraveller.Init(date);
        }
    }
}